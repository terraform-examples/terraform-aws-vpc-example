# Configure the AWS Provider
provider "aws" {
  version = "~> 2.0"
  region  = "us-east-1"
}

# Create a VPC
resource "aws_vpc" "example" {
  cidr_block = "172.16.0.0/16"
  tags = {
    Name = "example"
  }
}

resource "aws_subnet" "main" {
  vpc_id     = "${aws_vpc.example.id}"
  cidr_block = "172.16.64.0/16"

  tags = {
    Name = "example"
  }
}

output "vpc_id" {
  value = "${aws_vpc.example.id}"
}